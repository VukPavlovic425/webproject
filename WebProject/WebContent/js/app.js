const Registration = { template: '<registration></registration>' }
const MainPage = { template: '<main-page></main-page>' }

const router = new VueRouter({
	mode: 'hash',
	  routes: [
		{ path: '/', name: 'home', component: Registration},
		{ path: '/mainpage', name: 'mainpage', component: MainPage}
	  ]
});

var app = new Vue({
	router,
	el: '#id123'
});