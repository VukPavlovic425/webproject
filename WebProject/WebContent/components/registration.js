Vue.component("registration", {
	data: function () {
		    return {
		      user: { username:null, password:null, name:null, surname: null},
		      usernameColor: '',
		      passwordColor: '',
		      nameColor: '',
		      surnameColor: '',
		      error: ''
		    }
	},
	template: ` 
<div>
	<form>
		<label>Username</label>
		<input type = "text" v-model = "user.username" name = "username" v-bind:style="usernameColor">
		<label>Password</label>
		<input type = "text" v-model = "user.password" name = "password" v-bind:style="passwordColor">
		<label>Name</label>
		<input type = "text" v-model = "user.name" name = "name" v-bind:style="nameColor">
		<label>Surname</label>
		<input type = "text" v-model = "user.surname" name = "surname" v-bind:style="surnameColor">

		
		<input type = "submit" v-on:click = "createUser()" value = "Create">
		<p>{{error}}</p>
		
	</form>
</div>		  
`
	, 
	mounted () {
		
    },
	methods : {
		createUser : function () {
			event.preventDefault();
			if(!this.user.username ){
				this.usernameColor = 'background-color:red';
				
			}else{
				this.usernameColor = '';
			}
			
			if(!this.user.password ){
				this.passwordColor = 'background-color:red';
			}else{
				this.passwordColor = '';
			}
			if( !this.user.name){
				this.nameColor = 'background-color:red';
			}else{
				this.nameColor = '';
			}
			if( !this.user.surname ){
				this.surnameColor = 'background-color:red';
			}else{
				this.surnameColor = '';
			}
			
			
			
			if(!this.user.username || !this.user.password || !this.user.name || !this.user.surname ){
				this.error = 'error';
				return;
			}
			
			axios.post('rest/users', this.user).
			then(response => {
				router.push(`/mainpage`);
			});
			
		},
		
	}
});